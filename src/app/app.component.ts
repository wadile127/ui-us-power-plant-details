import { Component, OnInit  } from '@angular/core';
import { PostService } from '../services/post.service';

export class FilterInfo {
  public topPlants!: number;
  public state!: string;
}

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent implements OnInit {

  constructor(private service:PostService) {};

  ngOnInit() {
      this.service.getStates()
        .subscribe((response :any ) => {
          this.states = response;
        });
       this.service.getAllPlants()
          .subscribe((response : any ) => {
            this.powerPlants = response;
        }); 
  }
  stateName : string = '';
  // google maps zoom level
  zoom: number = 4;
  
  // initial center position for the map
  lat: number = 33.3881;
  lng: number = -112.8617;

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  onSubmit(form: any){
    console.log("Submit Clicked", form.value);

    this.service.getPlantsDetails(form.value)
          .subscribe((response : any ) => {
            this.powerPlants = response;
          });
    return null;
  }
  model = new FilterInfo();
  states : statesArray [] = [];
  

powerPlants: powerPlant [] = [];
}
// just an interface for type safety.
interface powerPlant {
  year : string;
  plantStateAbbreviation: string;
  plantName: string;
  plantLatitude: number;
  plantLongitude: number;
  plantAnnualNetGeneration : number;
  plantTotalNonrenewablesGenePercent: string;
  plantTotalCombustionGenerationPercent: string;
  id: string;
  plantTotalNoncombustionGenPercent: string;
  plantTotalNonhydroRenewablesGenPercent: string;
  plantTotalRenewablesGenPer: string;
	draggable: boolean;
}

interface statesArray {
  stateName : string;
  usAbbreviation: string;  
}